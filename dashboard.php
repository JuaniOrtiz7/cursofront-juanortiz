<?php 
include('db.php');

$query = "SELECT * FROM mensajes ";
$result = mysqli_query($conn,$query);
var_dump($result);
    var_dump(mysqli_error($conn))
?>

<?php include('header.php');?>

    <div class="container">
    <?php if (isset($_SESSION['message'])){ ?>
        <div class="row">
            <div class="col-sm-5 mx-auto">
                <div class="alert alert-<?php echo $_SESSION['message_type'];?> alert-dismissible fade show" role="alert">
                   <?php echo $_SESSION['message'] ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-sm-10 mx-auto">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>nombre</th>
                            <th>telefono</th>
                            <th>email</th>
                            <th>mensaje</th>
                            <th>fecha</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                        <?php while($row = mysqli_fetch_assoc($result)){ ?>
                        <tr>
                            <th><?php echo $row['id']; ?></th>
                            <th><?php echo $row['nombre']; ?></th>
                            <th><?php echo $row['telefono']; ?></th>
                            <th><?php echo $row['email']; ?></th>
                            <th><?php echo $row['mensaje']; ?></th>
                            <th><?php echo $row['created_at']; ?></th>
                            <th>
                                <a href="controller/ver.php?id=<?php echo $row['id']; ?>" class="btn btn-primary"><i class="bi-eye"></i></a>
                                <a href="controller/responder.php?id=<?php echo $row['id']; ?>" class="btn btn-primary"><i class="bi-reply"></i></a>
                                <a href="controller/eliminar.php?id=<?php echo $row['id']; ?>" class="btn btn-primary"><i class="bi-trash"></i></a>

                            </th>
                        </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>