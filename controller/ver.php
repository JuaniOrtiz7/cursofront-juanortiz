<?php 

include('../db.php');

$id = $_GET['id'];

$query = "SELECT * FROM `mensajes` WHERE id = $id";
$result = mysqli_query($conn, $query);

if (mysqli_num_rows($result) == 1){

   $row =  mysqli_fetch_array($result);

   $id = $row['id'];
   $nombre = $row['nombre'];
   $telefono = $row['telefono'];
   $email = $row['email'];
   $mensaje = $row['mensaje'];
   $fecha = $row['created_at'];
   $respuesta = $row['respuesta'];

}

include('../header.php');

?>



<div class="container">
    <div class="row">
        <div class="col-sm-5 mx-auto">
            <div class="card card-body">

                <h3 class="text-center">Cliente: <?php  echo $nombre; ?></h3>
                <h5 class="text-center">telefono: <?php  echo $telefono . ' - '. $email; ?></h5>
                <p class="text-center">Mensaje: <?php  echo $mensaje; ?></p>
                <hr>
                <?php if ($respuesta == null) { ?>
                <div class="row">
                    <div class="col-sm-5 mx-auto text-center">
                        <a href="responder.php?id=<?php echo $id;?>" class="btn btn-success"> Responder</a>
                    </div>
                </div>
                <?php } else { ?>
                <div class="col-sm-5 mx-auto text-center">
                    <p class="text-center">Respuesta: <?php  echo $respuesta; ?></p>

                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-sm-5 mx-auto text-center">
                        <a href="../dashboard.php" class="btn btn-outline-primary">Volver </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>