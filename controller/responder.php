<?php

include('../db.php');

if( isset($_GET['id'])){

    $id = $_GET['id'];

    
    $query = "SELECT * FROM `mensajes` WHERE id = $id";
    $result = mysqli_query($conn, $query);

    if (mysqli_num_rows($result) == 1){

        $row =  mysqli_fetch_array($result);
    
        $id = $row['id'];
        $nombre = $row['nombre'];

    }
}
if (isset($_POST['responder'])){

    $id = $_GET['id'];
    $respuesta = $_POST['mensaje'];

    $query = "UPDATE `mensajes` SET `respuesta`='$respuesta' WHERE id = '$id'";
    $result = mysqli_query($conn, $query);


    $_SESSION['message'] = 'Se envio correctamente la respuesta';
    $_SESSION['typo_message'] = 'info';

    header('Location:../dashboard.php');

}


include('../header.php')

?>
<div class="container p-4">
    <div class="row">
        <div class="col-md-4 mx-auto">
            <div class="card card-body">
                <form action="responder.php?id=<?php echo $id ?>" method="POST">
                    <div class="form-group">
                        <h5>Responder a <?php echo $nombre; ?></h5>
                        <div class="form-group">
                            <textarea name="mensaje" class="form-control mt-4" cols="30" rows="10"
                                placeholder="Mensaje..."></textarea>
                        </div>
                        <div class="row text-center mt-4">
                            <div class="col-sm-5 mx-auto">
                                <button class="btn btn-success" type="submit" name="responder">
                                    Responder
                                </button>
                            </div>
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>


</body>

</html>