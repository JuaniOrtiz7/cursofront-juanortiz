<?php session_start();?>
<!DOCTYPE <html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <title>CV Juani</title>

    <!-- Estilos de Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>

    <header>
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Juani</a>
                <div class="justify-content-end">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle
                            navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#info">Acerca de mí</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="#tecnologias">Mis Trabajos</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
        <!-- Header -->
        <div class="container mt-5">
            <h1 class="text-succes text-center mb-5" style="color:
                    aliceblue;"> HOLA MUNDO!</h1>
            <h2 class="text-succes text-center mb-5" style="color:
                    aliceblue;">¡Este es mi CV!</h2>
            <button id="btn" type="button" class="btn btn-dark" href="#form">CONTACTAME!</button>
        </div>


    </header>
    <!-- Info -->
    <section id="info">
        <div class="container mt-5">
            <div class="col-sm-8 mx-auto">
                <h1 class="text-center mb-5" id="titulo">Info</h1>
                <div class="accordion" id="accordionExample">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <strong>Edad</strong>
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse
                                collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body text-center">
                                Nací el 8 de diciembre del 2001. Tengo 19
                                años
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <strong>Idiomas</strong>
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse
                                collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body text-center mb-0">
                                Ingles Avanzado
                            </div>
                            <div class="accordion-body text-center">
                                A1 en Aleman
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingThree">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <strong>Educación</strong>
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse
                                collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                            <div class="accordion-body text-center">
                                Terciario en proceso
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="headingFour">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <strong>Pasatiempos</strong>
                            </button>
                        </h2>
                        <div id="collapseFour" class="accordion-collapse
                                collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                            <div class="accordion-body text-center mb-0">
                                Voy al gimnasio, toco la guitarra y juego al
                                fútbol americano
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Mis trabajos -->
    <section id="tecnologias">
        <div class="container ml-5 mr-5 pl-5 mr-5" >
            <h1 class="text-dark text-center pt-5 mt-5 mb-5">Mis trabajos</h1>
            <div class="row text-center mb-3">
                <div class="col-sm-4 text-center">
                    <div class="card m-5" style="border:none;">
                        <img src="assets/img/7.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">EGG Educación</h5>
                            <p class="card-text"></p>
                            <a href="https://github.com/jpLucero89/proyecto-final"
                                class="btn btn-outline-primary">Conocer más</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card m-5" style="border:none;">
                        <img src="assets/img/7.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">EGG Educación</h5>
                            <p class="card-text"></p>
                            <a href="webcliente.php"
                                class="btn btn-outline-primary">Conocer más</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section>
        <!-- Formulario de contacto -->
        <section id="contacto">

            <h1 class="text-center text-dark mt-5 pt-3 pb-4">Contacto</h1>
            <?php if (isset($_SESSION['message'])){ ?>
            <div class="row">
                <div class="col-sm-6 mx-auto">
                    <div class="alert alert-<?php echo $_SESSION['message_type'];?> alert-dismissible fade show"
                        role="alert">
                        <?php echo $_SESSION['message'] ?>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="container">
                <div class="col-sm-8 mx-auto">
                    <form action="controller/mensaje.php" method="POST">
                        <div class="d-flex">
                            <div class="col-sm-6 p-3">
                                <input type="text" class="form-control" name="nombre" placeholder="Nombre y Apellido"
                                    required>
                            </div>
                            <div class="col-sm-6 p-3">
                                <input type="number" class="form-control" name="telefono" placeholder="celular"
                                    required>
                            </div>
                        </div>
                        <div class="d-flex">
                            <input type="email" class="form-control m-3" name="email" placeholder="email" required>
                        </div>
                        <div class="d-flex">
                            <textarea id="" cols="30" rows="5" name="mensaje" placeholder="Tu Mensaje"
                                class="form-control m-3"></textarea>
                        </div>
                        <div class="d-flex m-5">
                            <button class="btn btn-success col-sm-3 mx-auto" name="enviar" type="submit">Enviar</button>
                        </div>
                </div>
                </form>
            </div>

        </section>

        <footer>
            <!-- Footer -->
            <nav id="contacto" class="navbar navbar-expand-lg navbar-dark
                bg-dark" aria-label="Tenth navbar example">
                <div class="container-fluid">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse
                        justify-content-md-center" id="navbarsExample08">
                        <ul class="navbar-nav">
                            <img src="assets/icons/instagram.svg" alt="Bootstrap" width="32" height="32">

                            <img src="assets/icons/facebook.svg" alt="Bootstrap" width="32" height="32">

                            <img src="assets/icons/linkedin.svg" alt="Bootstrap" width="32" height="32">
                        </ul>
                    </div>
                </div>
            </nav>
        </footer>


        <!-- Scripts Bootstrap -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
            integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous">
        </script>


</body>

</html>